﻿using System.Web.Mvc;
using BookStore.Domain.Abstract;
using BookStore.Domain.Entities;
using System.Linq;
using System.Web;

namespace BookStore.WebUI.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        IBookRepository repository;

        public AdminController(IBookRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index()
        {
            return View(repository.Books);
        }

        public ViewResult Edit(int BookId)
        {
            Book Book = repository.Books
                .FirstOrDefault(g => g.BookId == BookId);
            return View(Book);
        }

        // Перегруженная версия Edit() для сохранения изменений
        [HttpPost]
        public ActionResult Edit(Book Book, HttpPostedFileBase image = null)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    Book.ImageMimeType = image.ContentType;
                    Book.ImageData = new byte[image.ContentLength];
                    image.InputStream.Read(Book.ImageData, 0, image.ContentLength);
                }
                repository.SaveBook(Book);
                TempData["message"] = string.Format("Изменения в книге \"{0}\" были сохранены", Book.Name);
                return RedirectToAction("Index");
            }
            else
            {
                // Что-то не так со значениями данных
                return View(Book);
            }
        }

        public ViewResult Create()
        {
            return View("Edit", new Book());
        }

        [HttpPost]
        public ActionResult Delete(int BookId)
        {
            Book deletedBook = repository.DeleteBook(BookId);
            if (deletedBook != null)
            {
                TempData["message"] = string.Format("Книга \"{0}\" была удалена",
                    deletedBook.Name);
            }
            return RedirectToAction("Index");
        }
    }
}