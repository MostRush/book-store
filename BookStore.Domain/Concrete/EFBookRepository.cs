﻿using System.Collections.Generic;
using BookStore.Domain.Entities;
using BookStore.Domain.Abstract;

namespace BookStore.Domain.Concrete
{
    public class EFBookRepository : IBookRepository
    {
        EFDbContext context = new EFDbContext();
        public IEnumerable<Book> Books
        {
            get { return context.Books; }
        }
        public void SaveBook(Book Book)
        {
            if (Book.BookId == 0)
            {
                context.Books.Add(Book);
            }
            else
            {
                Book dbEntry = context.Books.Find(Book.BookId);
                if (dbEntry != null) {
                    dbEntry.Name = Book.Name;
                    dbEntry.Description = Book.Description;
                    dbEntry.Price = Book.Price;
                    dbEntry.Category = Book.Category;
                    dbEntry.ImageData = Book.ImageData;
                    dbEntry.ImageMimeType = Book.ImageMimeType;
                }
            } context.SaveChanges();
        }

        public Book DeleteBook(int BookId)
        {
            Book dbEntry = context.Books.Find(BookId);
            if (dbEntry != null)
            {
                context.Books.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}