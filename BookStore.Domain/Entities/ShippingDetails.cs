﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.Domain.Entities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Укажите как вас зовут!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите номер телефона!")]
        [Display(Name = "Укажите номер телефона:")]
        public string Line1 { get; set; }

        [Required(ErrorMessage = "Укажите номер клубной карты Book Store!")]
        [Display(Name = "Клубная карта библеотеки:")]
        public string City { get; set; }

        [Display(Name = "Введите дополнительную инфомарцию:")]
        public string Line2 { get; set; }
    }
}